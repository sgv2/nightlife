use rand::Rng;
use std::cmp;
use tcod::colors::*;
use tcod::console::*;
use tcod::input::{self, Event, Key, Mouse};
use tcod::map::{FovAlgorithm, Map as FovMap};

/////////////////////////////////////////////
//////// NON-ADJUSTABLE PROPERTIES //////////
/////////////////////////////////////////////

// window sizes
const SCREEN_WIDTH: i32 = 80;
const SCREEN_HEIGHT: i32 = 50;

// dungeon map sizes
const MAP_WIDTH: i32 = 80;
const MAP_HEIGHT: i32 = 43;

// GUI sizes and coordinates
const BAR_WIDTH: i32 = 20;
const PNL_HEIGHT: i32 = 7;
const PNL_Y: i32 = SCREEN_HEIGHT - PNL_HEIGHT;
const MSG_X: i32 = BAR_WIDTH + 2;
const MSG_WIDTH: i32 = SCREEN_WIDTH - BAR_WIDTH - 2;
const MSG_HEIGHT: usize = PNL_HEIGHT as usize - 1;
const INV_WIDTH: i32 = 50;

// player defaulted as first object
const PLAYER: usize = 0;

// player's vision parameters
const FOV_ALGO: FovAlgorithm = FovAlgorithm::Basic;
const LAMP_LIGHT_WALLS: bool = true;

/////////////////////////////////////////////
////////// ADJUSTABLE PROPERTIES ////////////
/////////////////////////////////////////////

// level to complete the game
const CLEAR: u32 = 10;

const LAMP_RADIUS: i32 = 10;

// 20 frames-per-second
const FPS: i32 = 20;

// relevant dungeon room parameters
const RM_MAX_SIZE: i32 = 10;
const RM_MIN_SIZE: i32 = 6;
const MAX_ROOMS: i32 = 30;

// relevant item parameters
const REFUEL_AMOUNT: i32 = 40;
const FLASHBANG_DMG: i32 = 99;
const FLASHBANG_RANGE: i32 = 5;
const BLIND_RANGE: i32 = 5;
const BLIND_TURNS: i32 = 10;

// dungeon environment
const DARK_WALL: Color = Color {
    r: 20,
    g: 20,
    b: 20,
};
const LIGHT_WALL: Color = Color {
    r: 50,
    g: 50,
    b: 30,
};
const DARK_GROUND: Color = Color {
    r: 30,
    g: 30,
    b: 30,
};
const LIGHT_GROUND: Color = Color {
    r: 80,
    g: 80,
    b: 60,
};

/////////////////////////////////////////////
///////////// MAIN OPERATIONS ///////////////
/////////////////////////////////////////////
fn main() {
    tcod::system::set_fps(FPS);

    // set window properties
    let root = Root::initializer()
        .font("arial10x10.png", FontLayout::Tcod)
        .font_type(FontType::Greyscale)
        .size(SCREEN_WIDTH, SCREEN_HEIGHT)
        .title("NightLife")
        .init();

    // store all game components
    let mut tcod = Tcod {
        root,
        main: Offscreen::new(MAP_WIDTH, MAP_HEIGHT),
        panel: Offscreen::new(SCREEN_WIDTH, PNL_HEIGHT),
        key: Default::default(),
        mouse: Default::default(),
        fov: FovMap::new(MAP_WIDTH, MAP_HEIGHT),
    };

    // start game
    main_menu(&mut tcod);
}

fn main_menu(tcod: &mut Tcod) {
    // background image
    let img = tcod::image::Image::from_file("background.png").expect("Background image not found");

    while !tcod.root.window_closed() {
        // render background
        tcod::image::blit_2x(&img, (0, 0), (-1, -1), &mut tcod.root, (0, 0));

        // title
        tcod.root.set_default_foreground(YELLOW);
        tcod.root.print_ex(
            SCREEN_WIDTH / 2,
            SCREEN_HEIGHT / 2 - 4,
            BackgroundFlag::None,
            TextAlignment::Center,
            "NightLife",
        );

        // menu
        let choices = &["Start", "Exit"];
        let choice = menu("", choices, 24, &mut tcod.root);

        match choice {
            // start game
            Some(0) => {
                let (mut game, mut objects) = new_game(tcod);
                play_game(tcod, &mut game, &mut objects);
            }
            // quit game
            Some(1) => {
                break;
            }
            _ => {}
        }
    }
}

fn new_game(tcod: &mut Tcod) -> (Game, Vec<Object>) {
    // create player
    let mut player = Object::new("player", '@', WHITE, 0, 0, true);
    player.alive = true;
    player.fighter = Some(Fighter {
        max_hp: 200,
        hp: 200,
        leech: 4,
        on_death: Death::Player,
    });

    // all objects in the game levels
    let mut objects = vec![player];

    // create the first level
    let mut game = Game {
        map: make_map(&mut objects, 1),
        messages: Msgs::new(),
        inventory: vec![],
        dungeon_level: 1,
    };

    // start with brightest lamp
    init_fov(tcod, &game.map);

    game.messages.add(
        format!(
            "You want to wake up in time for the big test but is stuck in a nightmare. Escape to the {}th level",
        CLEAR),
        ORANGE,
    );

    (game, objects)
}

fn play_game(tcod: &mut Tcod, game: &mut Game, objects: &mut Vec<Object>) {
    let mut prev_pos = (-1, -1);

    while !tcod.root.window_closed() {
        tcod.main.clear();

        // get information on player's keyboard and mouse
        match input::check_for_event(input::MOUSE | input::KEY_PRESS) {
            Some((_, Event::Mouse(m))) => tcod.mouse = m,
            Some((_, Event::Key(k))) => tcod.key = k,
            _ => tcod.key = Default::default(),
        }

        // create the screen
        let sight_change = prev_pos != (objects[PLAYER].pos());
        render(tcod, game, objects, sight_change);

        tcod.root.flush();

        // handle player's controls
        prev_pos = objects[PLAYER].pos();
        let action = handle_keys(tcod, game, objects);
        if action == PlayerAction::Exit {
            break;
        }

        // monster's turn to make an action
        if objects[PLAYER].alive && action != PlayerAction::DidntTakeTurn {
            for id in 0..objects.len() {
                if objects[id].ai.is_some() && objects[id].alive {
                    monster_turn(id, tcod, game, objects);
                }
            }
        }
    }
}

/////////////////////////////////////////////
///////////// SCREEN RENDERING //////////////
/////////////////////////////////////////////
// store all used libtcod components
struct Tcod {
    root: Root,       // the entire window
    main: Offscreen,  // dungeon map
    panel: Offscreen, // player GUI
    key: Key,         // key input handling
    mouse: Mouse,     // mouse handling
    fov: FovMap,      // vision for all living objects
}

// create the entire window with its components
fn render(tcod: &mut Tcod, game: &mut Game, objects: &[Object], sight_change: bool) {
    // redo lamp lighting for every step taken
    if sight_change {
        let player = &objects[PLAYER];
        if let Some(fighter) = player.fighter {
            let drain = fighter.max_hp / (fighter.hp + 1);
            let mut lamp = LAMP_RADIUS;

            if fighter.hp > 0 && drain < LAMP_RADIUS && drain > 0 {
                lamp = LAMP_RADIUS / drain;
            }
            if drain >= LAMP_RADIUS && fighter.hp > 0 {
                lamp = 1;
            }
            tcod.fov
                .compute_fov(player.x, player.y, lamp, LAMP_LIGHT_WALLS, FOV_ALGO);
        }
    }

    // render dungeon tiles
    for y in 0..MAP_HEIGHT {
        for x in 0..MAP_WIDTH {
            // determine what area is lit by player's lamp
            let visible = tcod.fov.is_in_fov(x, y);
            let wall = game.map[x as usize][y as usize].block_sight;
            let color = match (visible, wall) {
                (false, true) => DARK_WALL,
                (false, false) => DARK_GROUND,
                (true, true) => LIGHT_WALL,
                (true, false) => LIGHT_GROUND,
            };

            // player discovers new area
            let explored = &mut game.map[x as usize][y as usize].explored;
            if visible {
                *explored = true;
            }
            // show explored areas forever
            if *explored {
                tcod.main
                    .set_char_background(x, y, color, BackgroundFlag::Set);
            }
        }
    }

    // draw the objects on the tile
    let mut to_draw: Vec<_> = objects
        .iter()
        .filter(|o| {
            tcod.fov.is_in_fov(o.x, o.y)
                || (o.always_visible && game.map[o.x as usize][o.y as usize].explored)
        })
        .collect();
    // makes sure non-blocking objects are drawn at the top layer
    to_draw.sort_by(|o1, o2| o1.blocks.cmp(&o2.blocks));
    for object in &to_draw {
        object.draw(&mut tcod.main);
    }

    // combine main dungeon screen to the window
    blit(
        &tcod.main,
        (0, 0),
        (MAP_WIDTH, MAP_HEIGHT),
        &mut tcod.root,
        (0, 0),
        1.0,
        1.0,
    );

    tcod.panel.set_default_background(BLACK);
    tcod.panel.clear();

    // print game message
    let mut y = MSG_HEIGHT as i32;
    for &(ref msg, color) in game.messages.iter().rev() {
        let msg_height = tcod.panel.get_height_rect(MSG_X, y, MSG_WIDTH, 0, msg);
        y -= msg_height;
        if y < 0 {
            break;
        }
        tcod.panel.set_default_foreground(color);
        tcod.panel.print_rect(MSG_X, y, MSG_WIDTH, 0, msg);
    }

    // render player's lamp status
    let hp = objects[PLAYER].fighter.map_or(0, |f| f.hp);
    let max_hp = objects[PLAYER].fighter.map_or(0, |f| f.max_hp);
    render_bar(
        &mut tcod.panel,
        (1, 1),
        BAR_WIDTH,
        "LAMP",
        (hp, max_hp),
        LIGHT_RED,
        DARKER_RED,
    );

    tcod.panel.print_ex(
        1,
        3,
        BackgroundFlag::None,
        TextAlignment::Left,
        format!("Dungeon level: {}", game.dungeon_level),
    );

    // when hovering over object, show object's name
    tcod.panel.set_default_foreground(LIGHT_GREY);
    tcod.panel.print_ex(
        1,
        0,
        BackgroundFlag::None,
        TextAlignment::Left,
        hover_objects(tcod.mouse, objects, &tcod.fov),
    );

    // combine player's GUI screen to the window
    blit(
        &tcod.panel,
        (0, 0),
        (SCREEN_WIDTH, PNL_HEIGHT),
        &mut tcod.root,
        (0, PNL_Y),
        1.0,
        1.0,
    );
}

// generic menu window (main menu, inventory, etc.)
fn menu<T: AsRef<str>>(header: &str, options: &[T], width: i32, root: &mut Root) -> Option<usize> {
    assert!(
        options.len() <= 26,
        "Cannot have a menu with more than 26 options."
    );

    // get height for printing all choices and menu header
    let head_height = if header.is_empty() {
        0
    } else {
        root.get_height_rect(0, 0, width, SCREEN_HEIGHT, header)
    };
    let height = options.len() as i32 + head_height;

    // create menu screen
    let mut window = Offscreen::new(width, height);

    // print the menu
    window.set_default_foreground(YELLOW);
    window.print_rect_ex(
        0,
        0,
        width,
        height,
        BackgroundFlag::None,
        TextAlignment::Left,
        header,
    );
    for (index, option_text) in options.iter().enumerate() {
        let menu_letter = (b'a' + index as u8) as char;
        let text = format!("({}) {}", menu_letter, option_text.as_ref());
        window.print_ex(
            0,
            head_height + index as i32,
            BackgroundFlag::None,
            TextAlignment::Left,
            text,
        );
    }

    // combine menu screen to the window
    let x = SCREEN_WIDTH / 2 - width / 2;
    let y = SCREEN_HEIGHT / 2 - height / 2;
    blit(&window, (0, 0), (width, height), root, (x, y), 1.0, 0.7);

    // wait for player's input
    root.flush();
    let key = root.wait_for_keypress(true);

    // print menu options in alphabetical order
    if key.printable.is_alphabetic() {
        let index = key.printable.to_ascii_lowercase() as usize - 'a' as usize;
        if index < options.len() {
            Some(index)
        } else {
            None
        }
    } else {
        None
    }
}

// player's inventory
fn inv_menu(inv: &[Object], header: &str, root: &mut Root) -> Option<usize> {
    // display list of items as options
    let options = if inv.is_empty() {
        vec!["Inventory is empty.".into()]
    } else {
        inv.iter().map(|item| item.name.clone()).collect()
    };

    let inv_index = menu(header, &options, INV_WIDTH, root);

    // if an item was chosen, return it
    if !inv.is_empty() {
        inv_index
    } else {
        None
    }
}

// message system in player GUI
struct Msgs {
    messages: Vec<(String, Color)>,
}
impl Msgs {
    pub fn new() -> Self {
        Self { messages: vec![] }
    }

    // add message to GUI
    pub fn add<T: Into<String>>(&mut self, message: T, color: Color) {
        self.messages.push((message.into(), color));
    }

    // push message as the latest message
    pub fn iter(&self) -> impl DoubleEndedIterator<Item = &(String, Color)> {
        self.messages.iter()
    }
}

// player's lamp GUI
fn render_bar(
    panel: &mut Offscreen,
    coords: (i32, i32),
    total_width: i32,
    name: &str,
    hp: (i32, i32),
    bar_color: Color,
    back_color: Color,
) {
    let bar = (hp.0 as f32 / hp.1 as f32 * total_width as f32) as i32;
    panel.set_default_background(back_color);
    panel.rect(
        coords.0,
        coords.1,
        total_width,
        1,
        false,
        BackgroundFlag::Screen,
    );
    panel.set_default_background(bar_color);
    if bar > 0 {
        panel.rect(coords.0, coords.1, bar, 1, false, BackgroundFlag::Screen);
    }

    panel.set_default_foreground(WHITE);
    panel.print_ex(
        coords.0 + total_width / 2,
        coords.1,
        BackgroundFlag::None,
        TextAlignment::Center,
        name.to_string(),
    );
}

// list of objects that can be hovered over
fn hover_objects(mouse: Mouse, objects: &[Object], fov_map: &FovMap) -> String {
    let (x, y) = (mouse.cx as i32, mouse.cy as i32);

    // get all objects in player's sight
    let names = objects
        .iter()
        .filter(|obj| obj.pos() == (x, y) && fov_map.is_in_fov(obj.x, obj.y))
        .map(|obj| obj.name.clone())
        .collect::<Vec<_>>();

    names.join(", ")
}

/////////////////////////////////////////////
/////////////// MAP CREATION ////////////////
/////////////////////////////////////////////

// holds the entire dungeon info
struct Game {
    map: Map,
    messages: Msgs,
    inventory: Vec<Object>,
    dungeon_level: u32,
}

// dungeon map layout
type Map = Vec<Vec<Tile>>;

// a tile of the dungeon
#[derive(Clone, Copy, Debug)]
struct Tile {
    blocked: bool,
    explored: bool,
    block_sight: bool,
}
impl Tile {
    // can't walk through
    pub fn wall() -> Self {
        Tile {
            explored: false,
            block_sight: true,
            blocked: true,
        }
    }

    // walkable
    pub fn empty() -> Self {
        Tile {
            explored: false,
            block_sight: false,
            blocked: false,
        }
    }
}

// dungeon room
#[derive(Clone, Copy, Debug)]
struct Room {
    x1: i32,
    x2: i32,
    y1: i32,
    y2: i32,
}
impl Room {
    // room positioned from one corner to another
    pub fn new(x: i32, y: i32, w: i32, h: i32) -> Self {
        Room {
            x1: x,
            x2: x + w,
            y1: y,
            y2: y + h,
        }
    }

    // get the center coordinate of the room
    pub fn center(&self) -> (i32, i32) {
        let center_x = (self.x1 + self.x2) / 2;
        let center_y = (self.y1 + self.y2) / 2;
        (center_x, center_y)
    }

    // check if a room intersects another
    pub fn intersection(&self, other: &Room) -> bool {
        (self.x1 <= other.x2)
            && (self.x2 >= other.x1)
            && (self.y1 <= other.y2)
            && (self.y2 >= other.y1)
    }
}

// room filled with walkable tiles
fn create_rm(room: Room, map: &mut Map) {
    for x in (room.x1 + 1)..room.x2 {
        for y in (room.y1 + 1)..room.y2 {
            map[x as usize][y as usize] = Tile::empty();
        }
    }
}

// walkable horizontal path
fn create_hpath(x1: i32, x2: i32, y: i32, map: &mut Map) {
    for x in cmp::min(x1, x2)..(cmp::max(x1, x2) + 1) {
        map[x as usize][y as usize] = Tile::empty();
    }
}

// walkable vertical path
fn create_vpath(y1: i32, y2: i32, x: i32, map: &mut Map) {
    for y in cmp::min(y1, y2)..(cmp::max(y1, y2) + 1) {
        map[x as usize][y as usize] = Tile::empty();
    }
}

// create a dungeon layout full of objects
fn make_map(objects: &mut Vec<Object>, level: u32) -> Map {
    let mut map = vec![vec![Tile::wall(); MAP_HEIGHT as usize]; MAP_WIDTH as usize];

    assert_eq!(&objects[PLAYER] as *const _, &objects[0] as *const _);
    objects.truncate(1);

    let mut rooms = vec![];
    for _ in 0..MAX_ROOMS {
        // size of room
        let w = rand::thread_rng().gen_range(RM_MIN_SIZE, RM_MAX_SIZE + 1);
        let h = rand::thread_rng().gen_range(RM_MIN_SIZE, RM_MAX_SIZE + 1);

        // coordinates to place room
        let x = rand::thread_rng().gen_range(0, MAP_WIDTH - w);
        let y = rand::thread_rng().gen_range(0, MAP_HEIGHT - h);

        // makes sure new room doesn't overlap with other rooms
        let new_room = Room::new(x, y, w, h);
        let failed = rooms
            .iter()
            .any(|other_room| new_room.intersection(other_room));

        if !failed {
            create_rm(new_room, &mut map);

            place_objs(new_room, &map, objects, level);

            let (new_x, new_y) = new_room.center();

            // player's spawn point is the first made room
            if rooms.is_empty() {
                objects[PLAYER].set_pos(new_x, new_y);
            }
            // start connecting new rooms made with other rooms
            else {
                let (prev_x, prev_y) = rooms[rooms.len() - 1].center();

                // randomly start path horizontally or vertically
                if rand::random() {
                    create_hpath(prev_x, new_x, prev_y, &mut map);
                    create_vpath(prev_y, new_y, new_x, &mut map);
                } else {
                    create_vpath(prev_y, new_y, prev_x, &mut map);
                    create_hpath(prev_x, new_x, new_y, &mut map);
                }
            }

            rooms.push(new_room);
        }
    }

    // exit in the last room made
    let (last_x, last_y) = rooms[rooms.len() - 1].center();
    let mut stairs = Object::new("stairs", '<', WHITE, last_x, last_y, false);
    stairs.always_visible = true;
    objects.push(stairs);

    map
}

// set lamp brightness (player's vision)
fn init_fov(tcod: &mut Tcod, map: &Map) {
    for y in 0..MAP_HEIGHT {
        for x in 0..MAP_WIDTH {
            tcod.fov.set(
                x,
                y,
                !map[x as usize][y as usize].block_sight,
                !map[x as usize][y as usize].blocked,
            );
        }
    }

    // start with areas outside of the player's sight dark
    tcod.main.clear();
}

// enter the next level after descending the stairs
fn next_level(tcod: &mut Tcod, game: &mut Game, objects: &mut Vec<Object>) {
    // recover fuel upon next level
    game.messages.add(
        "You walk down the stairs and notice your lamp has gotten brighter",
        YELLOW,
    );
    let lamp_fuel = objects[PLAYER].fighter.map_or(0, |f| f.max_hp / 2);
    objects[PLAYER].refuel(lamp_fuel);

    game.messages.add(
        "Entering the new room you get the feeling \
        that the worst has yet to come...",
        ORANGE,
    );

    // player has reached the end of the game
    if game.dungeon_level >= CLEAR - 1 {
        if let Some(fighter) = objects[0].fighter {
            fighter.on_death.kill(&mut objects[0], game);
        }
    }

    game.dungeon_level += 1;
    game.map = make_map(objects, game.dungeon_level);
    init_fov(tcod, &game.map);
}

// generic rate of occurences for objects upon further levels
struct Transition {
    level: u32,
    value: u32,
}

// helps determine proportion of differet object types for each room
fn from_dunglvl(table: &[Transition], level: u32) -> u32 {
    table
        .iter()
        .rev()
        .find(|transition| level >= transition.level)
        .map_or(0, |transition| transition.value)
}

// place all objects based on rate of occurences
fn place_objs(room: Room, map: &Map, objects: &mut Vec<Object>, level: u32) {
    use rand::distributions::{IndependentSample, Weighted, WeightedChoice};

    // max monsters for the room
    let max_mon = from_dunglvl(
        &[
            Transition { level: 1, value: 2 },
            Transition { level: 5, value: 3 },
            Transition { level: 8, value: 4 },
        ],
        level,
    );

    // random # of monsters
    let num_monsters = rand::thread_rng().gen_range(0, max_mon + 1);

    // chances of monster type occuring for each level section
    let nr_chance = from_dunglvl(
        &[
            Transition {
                level: 3,
                value: 15,
            },
            Transition {
                level: 5,
                value: 30,
            },
            Transition {
                level: 7,
                value: 60,
            },
        ],
        level,
    );
    let ml_chance = from_dunglvl(
        &[
            Transition { level: 3, value: 7 },
            Transition {
                level: 5,
                value: 15,
            },
            Transition {
                level: 7,
                value: 30,
            },
        ],
        level,
    );
    let sc_chance = from_dunglvl(
        &[
            Transition { level: 3, value: 3 },
            Transition { level: 5, value: 7 },
            Transition {
                level: 7,
                value: 15,
            },
        ],
        level,
    );

    // draw one of the monster type to place
    let monster_chances = &mut [
        Weighted {
            weight: 80,
            item: "Blighted Mosquito",
        },
        Weighted {
            weight: nr_chance,
            item: "Night Reaver",
        },
        Weighted {
            weight: ml_chance,
            item: "Mangled Lurker",
        },
        Weighted {
            weight: sc_chance,
            item: "Shade Crawler",
        },
    ];
    let monster_choice = WeightedChoice::new(monster_chances);

    // begin randomly placing monsters
    for _ in 0..num_monsters {
        let x = rand::thread_rng().gen_range(room.x1 + 1, room.x2);
        let y = rand::thread_rng().gen_range(room.y1 + 1, room.y2);

        if !is_blocked(x, y, map, objects) {
            let mut monster = match monster_choice.ind_sample(&mut rand::thread_rng()) {
                "Blighted Mosquito" => {
                    let mut bm = Object::new("Blighted Mosquito", 'm', LIGHT_GREEN, x, y, true);
                    bm.fighter = Some(Fighter {
                        max_hp: 20,
                        hp: 20,
                        leech: 4,
                        on_death: Death::Monster,
                    });
                    bm.ai = Some(Ai::Basic);
                    bm
                }
                "Night Reaver" => {
                    let mut nr = Object::new("Night Reaver", 'n', DARKER_SEPIA, x, y, true);
                    nr.fighter = Some(Fighter {
                        max_hp: 30,
                        hp: 30,
                        leech: 8,
                        on_death: Death::Monster,
                    });
                    nr.ai = Some(Ai::Basic);
                    nr
                }
                "Mangled Lurker" => {
                    let mut ml = Object::new("Mangled Lurker", 'M', DARKER_LIME, x, y, true);
                    ml.fighter = Some(Fighter {
                        max_hp: 30,
                        hp: 30,
                        leech: 15,
                        on_death: Death::Monster,
                    });
                    ml.ai = Some(Ai::Basic);
                    ml
                }
                "Shade Crawler" => {
                    let mut sc = Object::new("Shade Crawler", 'S', DARKEST_AMBER, x, y, true);
                    sc.fighter = Some(Fighter {
                        max_hp: 30,
                        hp: 30,
                        leech: 20,
                        on_death: Death::Monster,
                    });
                    sc.ai = Some(Ai::Basic);
                    sc
                }
                _ => unreachable!(),
            };
            monster.alive = true;
            objects.push(monster);
        }
    }

    // max items for the room
    let max_items = from_dunglvl(
        &[
            Transition { level: 1, value: 1 },
            Transition { level: 4, value: 2 },
        ],
        level,
    );

    // chances of item type occuring for each level
    let item_chances = &mut [
        Weighted {
            weight: 35,
            item: Item::Fuel,
        },
        Weighted {
            weight: from_dunglvl(
                &[Transition {
                    level: 4,
                    value: 10,
                }],
                level,
            ),
            item: Item::Flashbang,
        },
        Weighted {
            weight: from_dunglvl(
                &[Transition {
                    level: 2,
                    value: 25,
                }],
                level,
            ),
            item: Item::Blackout,
        },
    ];
    let item_choice = WeightedChoice::new(item_chances);

    // random # of items
    let num_items = rand::thread_rng().gen_range(0, max_items + 1);

    for _ in 0..num_items {
        // choose random spot for this item
        let x = rand::thread_rng().gen_range(room.x1 + 1, room.x2);
        let y = rand::thread_rng().gen_range(room.y1 + 1, room.y2);

        // only place it if the tile is not blocked
        if !is_blocked(x, y, map, objects) {
            let mut item = match item_choice.ind_sample(&mut rand::thread_rng()) {
                Item::Fuel => {
                    // create a healing potion
                    let mut object = Object::new("lamp fuel", '!', VIOLET, x, y, false);
                    object.item = Some(Item::Fuel);
                    object
                }
                Item::Flashbang => {
                    // create a lightning bolt scroll
                    let mut object =
                        Object::new("flashbang grenade", '6', LIGHT_YELLOW, x, y, false);
                    object.item = Some(Item::Flashbang);
                    object
                }
                Item::Blackout => {
                    // create a confuse scroll
                    let mut object = Object::new("blackout bomb", '0', DARKEST_ORANGE, x, y, false);
                    object.item = Some(Item::Blackout);
                    object
                }
            };
            item.always_visible = true;
            objects.push(item);
        }
    }
}

/////////////////////////////////////////////
//////////// GENERIC OBJECT /////////////////
/////////////////////////////////////////////
#[derive(Debug)]
struct Object {
    name: String,
    symbol: char,
    color: Color,
    alive: bool,
    x: i32,
    y: i32,
    blocks: bool,
    fighter: Option<Fighter>,
    ai: Option<Ai>,
    item: Option<Item>,
    always_visible: bool,
}
impl Object {
    pub fn new(name: &str, sym: char, colr: Color, dx: i32, dy: i32, dblocks: bool) -> Self {
        Object {
            name: name.into(),
            symbol: sym,
            color: colr,
            alive: false,
            x: dx,
            y: dy,
            blocks: dblocks,
            fighter: None,
            ai: None,
            item: None,
            always_visible: false,
        }
    }

    // set and draw the object on the map
    pub fn draw(&self, main: &mut dyn Console) {
        main.set_default_foreground(self.color);
        main.put_char(self.x, self.y, self.symbol, BackgroundFlag::None);
    }

    pub fn set_pos(&mut self, x: i32, y: i32) {
        self.x = x;
        self.y = y;
    }

    pub fn pos(&self) -> (i32, i32) {
        (self.x, self.y)
    }

    // distance to clicked object (for player only)
    pub fn distance(&self, x: i32, y: i32) -> f32 {
        (((x - self.x).pow(2) + (y - self.y).pow(2)) as f32).sqrt()
    }

    // get distance between two objects
    pub fn distance_to(&self, other: &Object) -> f32 {
        let x = other.x - self.x;
        let y = other.y - self.y;
        ((x.pow(2) + y.pow(2)) as f32).sqrt()
    }

    // fill lamp fuel up to amount
    pub fn refuel(&mut self, amount: i32) {
        if let Some(ref mut fighter) = self.fighter {
            fighter.hp += amount;
            if fighter.hp > fighter.max_hp {
                fighter.hp = fighter.max_hp;
            }
        }
    }

    // for player only: reduce lamp fuel
    pub fn reduce_fuel(&mut self, damage: i32, game: &mut Game) {
        if let Some(fighter) = self.fighter.as_mut() {
            if damage > 0 {
                fighter.hp -= damage;
            }
        }

        // living object died
        if let Some(fighter) = self.fighter {
            if fighter.hp <= 0 {
                self.alive = false;
                fighter.on_death.kill(self, game);
            }
        }
    }

    // for monsters only: take a portion of the player's lamp fuel
    pub fn attack(&mut self, target: &mut Object, game: &mut Game) {
        let dmg = self.fighter.map_or(0, |f| f.leech);
        if dmg > 0 {
            // make monster disappear after attacking
            if let Some(fighter) = self.fighter {
                self.alive = false;
                fighter.on_death.kill(self, game);
            }
            target.reduce_fuel(dmg, game);
        }
    }
}

// move object to walkable tile
fn move_by(id: usize, dx: i32, dy: i32, objects: &mut [Object], game: &mut Game) {
    let (x, y) = objects[id].pos();
    if !is_blocked(x + dx, y + dy, &game.map, objects) {
        objects[id].set_pos(x + dx, y + dy);

        // player's lamp drain for each step taken
        if id == 0 {
            objects[id].reduce_fuel(1, game);
        }
    }
}

// create two borrowable mut elements from object
fn mut_two<T>(first: usize, second: usize, objects: &mut [T]) -> (&mut T, &mut T) {
    assert!(first != second);
    let split = cmp::max(first, second);
    let (slice1, slice2) = objects.split_at_mut(split);
    if first < second {
        (&mut slice1[first], &mut slice2[0])
    } else {
        (&mut slice2[0], &mut slice1[second])
    }
}

// check if tile to walk to is non-walkable
fn is_blocked(x: i32, y: i32, map: &Map, objects: &[Object]) -> bool {
    if map[x as usize][y as usize].blocked {
        return true;
    }

    objects
        .iter()
        .any(|object| object.blocks && object.pos() == (x, y))
}

// attack properties for living objects
#[derive(Clone, Copy, Debug, PartialEq)]
struct Fighter {
    max_hp: i32,
    hp: i32,
    leech: i32,
    on_death: Death,
}

// death for living objects
#[derive(Clone, Copy, Debug, PartialEq)]
enum Death {
    Player,
    Monster,
}
impl Death {
    fn kill(self, object: &mut Object, game: &mut Game) {
        use Death::*;

        let kill = match self {
            Player => player_death,
            Monster => monster_death,
        };

        kill(object, game);
    }
}

fn player_death(player: &mut Object, game: &mut Game) {
    // the game ended!
    game.messages.add("You woke up...", ORANGE);
    if game.dungeon_level >= CLEAR - 1 && player.alive {
        player.alive = false;
        game.messages
            .add("In time to make it for the test!", YELLOW);
        player.symbol = 'W';
        player.color = WHITE;
    } else {
        game.messages.add("Late and missed the big test!", RED);
        player.symbol = '%';
        player.color = DARK_RED;

        // reveal the map
        for y in 0..MAP_HEIGHT {
            for x in 0..MAP_WIDTH {
                let explored = &mut game.map[x as usize][y as usize].explored;
                *explored = true;
            }
        }
    }

    game.messages.add("Press Esc to enter the menu", ORANGE);
}

// make monster disappear
fn monster_death(monster: &mut Object, game: &mut Game) {
    game.messages.add(
        format!("{} takes some light and disappears", monster.name,),
        RED,
    );
    monster.symbol = ' ';
    monster.color = DARK_RED;
    monster.blocks = false;
    monster.fighter = None;
    monster.ai = None;
    monster.name = format!("traces of {}", monster.name);
}

/////////////////////////////////////////////
//////////// MONSTER BEHAVIOR ///////////////
/////////////////////////////////////////////

// basic AI for monsters
#[derive(Clone, Debug, PartialEq)]
enum Ai {
    Basic,
    Blinded { num_turns: i32, prev_ai: Box<Ai> },
}

// monster chases player or attempt to regain vision
fn monster_turn(monster_id: usize, tcod: &Tcod, game: &mut Game, objects: &mut [Object]) {
    use Ai::*;
    if let Some(ai) = objects[monster_id].ai.take() {
        let new_ai = match ai {
            Basic => ai_basic(monster_id, tcod, game, objects),
            Blinded { num_turns, prev_ai } => {
                ai_blinded(monster_id, tcod, game, objects, prev_ai, num_turns)
            }
        };

        objects[monster_id].ai = Some(new_ai);
    }
}

// monsters following player
fn chase(id: usize, px: i32, py: i32, objects: &mut [Object], game: &mut Game) {
    // get vector from monster to player
    let x2 = px - objects[id].x;
    let y2 = py - objects[id].y;
    let dist = ((x2.pow(2) + y2.pow(2)) as f32).sqrt();

    // convert vectors into one tile step
    let dx = (x2 as f32 / dist).round() as i32;
    let dy = (y2 as f32 / dist).round() as i32;
    move_by(id, dx, dy, objects, game);
}

// monster's mechanic towards the player
fn ai_basic(monster_id: usize, tcod: &Tcod, game: &mut Game, objects: &mut [Object]) -> Ai {
    let (monster_x, monster_y) = objects[monster_id].pos();

    // chase the player if monster is in the light source
    if tcod.fov.is_in_fov(monster_x, monster_y) {
        if objects[monster_id].distance_to(&objects[PLAYER]) >= 2.0 {
            let (player_x, player_y) = objects[PLAYER].pos();
            chase(monster_id, player_x, player_y, objects, game);
        }
        // monster can leech player's lamp when close enough
        else if objects[PLAYER].fighter.map_or(false, |f| f.hp > 0) {
            let (monster, player) = mut_two(monster_id, PLAYER, objects);
            monster.attack(player, game);
        }
    }

    Ai::Basic
}

// monster hit by blackout bomb (cannot attack player)
fn ai_blinded(
    monster_id: usize,
    _tcod: &Tcod,
    game: &mut Game,
    objects: &mut [Object],
    prev: Box<Ai>,
    num_turns: i32,
) -> Ai {
    if num_turns >= 0 {
        // move randomly until able to regain sight
        move_by(
            monster_id,
            rand::thread_rng().gen_range(-1, 2),
            rand::thread_rng().gen_range(-1, 2),
            objects,
            game,
        );

        Ai::Blinded {
            prev_ai: prev,
            num_turns: num_turns - 1,
        }
    }
    // regain sight and resume normal behavior
    else {
        game.messages.add(
            format!("The {} is no longer blinded!", objects[monster_id].name),
            RED,
        );

        *prev
    }
}

/////////////////////////////////////////////
///////////// PLAYER ACTIONS ////////////////
/////////////////////////////////////////////
#[derive(Clone, Copy, Debug, PartialEq)]
enum PlayerAction {
    TookTurn,
    DidntTakeTurn,
    Exit,
}

// player attempts to pick up item from the map
fn grab_item(item_id: usize, game: &mut Game, objects: &mut Vec<Object>) {
    if game.inventory.len() >= 10 {
        game.messages.add(
            format!(
                "Inventory is full, unable to grab {}",
                objects[item_id].name
            ),
            RED,
        );
    } else {
        let item = objects.swap_remove(item_id);
        game.messages
            .add(format!("You grabbed a {}", item.name), YELLOW);
        game.inventory.push(item);
    }
}

// allow player to drop an item
fn drop_item(inv_id: usize, game: &mut Game, objects: &mut Vec<Object>) {
    let mut item = game.inventory.remove(inv_id);
    item.set_pos(objects[PLAYER].x, objects[PLAYER].y);

    game.messages
        .add(format!("You dropped a {}.", item.name), RED);

    // place dropped item into objects in the dungeon
    objects.push(item);
}

fn handle_keys(tcod: &mut Tcod, game: &mut Game, objects: &mut Vec<Object>) -> PlayerAction {
    use tcod::input::KeyCode::*;
    use PlayerAction::*;

    let player_alive = objects[PLAYER].alive;
    match (tcod.key, tcod.key.text(), player_alive) {
        // full screen
        (
            Key {
                code: Enter,
                alt: true,
                ..
            },
            _,
            _,
        ) => {
            let fullscreen = tcod.root.is_fullscreen();
            tcod.root.set_fullscreen(!fullscreen);
            DidntTakeTurn
        }

        // exit game
        (Key { code: Escape, .. }, _, _) => Exit,

        // movement
        (Key { code: Up, .. }, _, true) | (Key { code: NumPad8, .. }, _, true) => {
            move_by(PLAYER, 0, -1, objects, game);
            TookTurn
        }
        (Key { code: Down, .. }, _, true) | (Key { code: NumPad2, .. }, _, true) => {
            move_by(PLAYER, 0, 1, objects, game);
            TookTurn
        }
        (Key { code: Left, .. }, _, true) | (Key { code: NumPad4, .. }, _, true) => {
            move_by(PLAYER, -1, 0, objects, game);
            TookTurn
        }
        (Key { code: Right, .. }, _, true) | (Key { code: NumPad6, .. }, _, true) => {
            move_by(PLAYER, 1, 0, objects, game);
            TookTurn
        }
        (Key { code: Home, .. }, _, true) | (Key { code: NumPad7, .. }, _, true) => {
            move_by(PLAYER, -1, -1, objects, game);
            TookTurn
        }
        (Key { code: PageUp, .. }, _, true) | (Key { code: NumPad9, .. }, _, true) => {
            move_by(PLAYER, 1, -1, objects, game);
            TookTurn
        }
        (Key { code: End, .. }, _, true) | (Key { code: NumPad1, .. }, _, true) => {
            move_by(PLAYER, -1, 1, objects, game);
            TookTurn
        }
        (Key { code: PageDown, .. }, _, true) | (Key { code: NumPad3, .. }, _, true) => {
            move_by(PLAYER, 1, 1, objects, game);
            TookTurn
        }

        // wait
        (Key { code: NumPad5, .. }, _, true) => TookTurn,

        // grab item
        (Key { code: Text, .. }, "f", true) => {
            let item_id = objects
                .iter()
                .position(|object| object.pos() == objects[PLAYER].pos() && object.item.is_some());
            if let Some(item_id) = item_id {
                grab_item(item_id, game, objects);
            }
            DidntTakeTurn
        }

        // use item
        (Key { code: Text, .. }, "d", true) => {
            let inv_index = inv_menu(
                &game.inventory,
                "Use an item by pressing the key next to the name.\n",
                &mut tcod.root,
            );
            if let Some(inv_index) = inv_index {
                use_item(inv_index, tcod, game, objects);
            }
            DidntTakeTurn
        }

        // drop item
        (Key { code: Text, .. }, "e", true) => {
            let inv_index = inv_menu(
                &game.inventory,
                "Drop an item by pressing the key next to the name\n'",
                &mut tcod.root,
            );
            if let Some(inv_index) = inv_index {
                drop_item(inv_index, game, objects);
            }
            DidntTakeTurn
        }

        // transition to next level if on the stairs
        (Key { code: Spacebar, .. }, _, true) => {
            let player_on_stairs = objects
                .iter()
                .any(|object| object.pos() == objects[PLAYER].pos() && object.name == "stairs");
            if player_on_stairs {
                next_level(tcod, game, objects);
            }
            DidntTakeTurn
        }

        _ => DidntTakeTurn,
    }
}

/////////////////////////////////////////////
////////////// PLAYER ITEMS /////////////////
/////////////////////////////////////////////

#[derive(Clone, Copy, Debug, PartialEq)]
enum Item {
    Fuel,
    Flashbang,
    Blackout,
}

// state on item to be used
enum Using {
    Used,
    Cancelled,
}

// generic item use
fn use_item(inv_id: usize, tcod: &mut Tcod, game: &mut Game, objects: &mut [Object]) {
    use Item::*;

    if let Some(item) = game.inventory[inv_id].item {
        // call either use functions from selected item
        let on_use = match item {
            Fuel => use_refuel,
            Blackout => use_blackout,
            Flashbang => use_flashbang,
        };

        // determine what to do in the inventory
        match on_use(inv_id, tcod, game, objects) {
            Using::Used => {
                game.inventory.remove(inv_id);
            }

            Using::Cancelled => {
                game.messages.add("Inventory Closed", ORANGE);
            }
        }
    }
    // error case if item is invalid
    else {
        game.messages
            .add(format!("Cannot use {}", game.inventory[inv_id].name), RED);
    }
}

// player's healing
fn use_refuel(_inv_id: usize, _tcod: &mut Tcod, game: &mut Game, objects: &mut [Object]) -> Using {
    if let Some(fighter) = objects[PLAYER].fighter {
        // lamp is at its fullest
        if fighter.hp == fighter.max_hp {
            game.messages.add("The lamp can't take any more fuel", RED);
            return Using::Cancelled;
        }

        // fill player's lamp
        game.messages.add("Your lamp has gotten brighter", YELLOW);

        objects[PLAYER].refuel(REFUEL_AMOUNT);
        return Using::Used;
    }
    Using::Cancelled
}

// player's attack on monsters
fn use_flashbang(
    _inv_id: usize,
    tcod: &mut Tcod,
    game: &mut Game,
    objects: &mut [Object],
) -> Using {
    // check for monster player can hit with flashbang
    let monster_id = closest_monster(tcod, objects, FLASHBANG_RANGE);

    // found a monster in range
    if let Some(monster_id) = monster_id {
        game.messages.add(
            format!(
                "A flash of light engulfs the {}. \
                 It disappears after the loud bang",
                objects[monster_id].name
            ),
            LIGHT_BLUE,
        );

        // kill the monster
        objects[monster_id].reduce_fuel(FLASHBANG_DMG, game);
        Using::Used
    }
    // no monster in throwing range
    else {
        game.messages
            .add("No monster is near you to use the grenade", RED);
        Using::Cancelled
    }
}

// player's temporary stun on monster
fn use_blackout(_inv_id: usize, tcod: &mut Tcod, game: &mut Game, objects: &mut [Object]) -> Using {
    // prompt for player's input
    game.messages.add(
        "Left-click an enemy to blind it, or right-click to cancel.",
        ORANGE,
    );

    let monster_id = find_mon(tcod, game, objects, Some(BLIND_RANGE as f32));

    // blind the monster selected
    if let Some(monster_id) = monster_id {
        let old_ai = objects[monster_id].ai.take().unwrap_or(Ai::Basic);

        // replace current monster behavior
        objects[monster_id].ai = Some(Ai::Blinded {
            prev_ai: Box::new(old_ai),
            num_turns: BLIND_TURNS,
        });

        game.messages.add(
            format!(
                "The {} is covered in black soot, unable to see you.",
                objects[monster_id].name
            ),
            LIGHT_BLUE,
        );
        Using::Used
    }
    // monster is out of range
    else {
        game.messages
            .add("No enemy is close enough to strike.", RED);
        Using::Cancelled
    }
}

// get the position of what's left-clicked when prompted
fn get_tile(
    tcod: &mut Tcod,
    game: &mut Game,
    objects: &[Object],
    range: Option<f32>,
) -> Option<(i32, i32)> {
    use tcod::input::KeyCode::Escape;
    loop {
        // display name of object mouse hovers over
        tcod.root.flush();
        let event = input::check_for_event(input::KEY_PRESS | input::MOUSE).map(|e| e.1);
        match event {
            Some(Event::Mouse(m)) => tcod.mouse = m,
            Some(Event::Key(k)) => tcod.key = k,
            None => tcod.key = Default::default(),
        }
        render(tcod, game, objects, false);

        let (x, y) = (tcod.mouse.cx as i32, tcod.mouse.cy as i32);

        // targeted tile is in range and is a valid object
        let in_sight = (x < MAP_WIDTH) && (y < MAP_HEIGHT) && tcod.fov.is_in_fov(x, y);
        let in_range = range.map_or(true, |range| objects[PLAYER].distance(x, y) <= range);
        if tcod.mouse.lbutton_pressed && in_sight && in_range {
            return Some((x, y));
        }

        // exit out of selection
        if !in_range || tcod.mouse.rbutton_pressed || tcod.key.code == Escape {
            return None;
        }
    }
}

// prompt player to select a monster
fn find_mon(
    tcod: &mut Tcod,
    game: &mut Game,
    objects: &[Object],
    range: Option<f32>,
) -> Option<usize> {
    // allows viewing of name view mouse hovering until monster is selected or cancelled
    loop {
        match get_tile(tcod, game, objects, range) {
            Some((x, y)) => {
                for (id, obj) in objects.iter().enumerate() {
                    if obj.pos() == (x, y) && obj.fighter.is_some() && id != PLAYER {
                        return Some(id);
                    }
                }
            }
            None => return None,
        }
    }
}

// player's proximity attack with flashbang
fn closest_monster(tcod: &Tcod, objects: &[Object], range: i32) -> Option<usize> {
    let mut monster = None;
    let mut shortest = (range) as f32;

    // checkk for all objects in dungeon
    for (id, object) in objects.iter().enumerate() {
        // make sure it's a monster
        if (id != PLAYER)
            && object.fighter.is_some()
            && object.ai.is_some()
            && tcod.fov.is_in_fov(object.x, object.y)
        {
            // track the closest monster
            let dist = objects[PLAYER].distance_to(object);
            if dist <= shortest {
                monster = Some(id);
                shortest = dist;
            }
        }
    }

    monster
}
