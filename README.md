# Nightlife | Project for CS 410P: Programming in Rust

The game is about the player who wants to sleep in peace. Unfortunately the player is having a nightmare where they are trapped in a dark dungeon. The player must proceed through each dungeon floor by finding the exit for the corresponding floors. There is lurking danger that makes this escaping the dungeon more difficult. The player must carry a lamp to traverse through the corridor. Without light the player will immediately wake up and lose the game. What the player must be cautious of is keeping their light on at all times, so players can find items that will benefit them in escaping the dungeon floors quicker. That also means there are other opportunities for them to lose more light. The player will need to exit all of the dungeon floors in order to have a good night rest or else they will be staying up all night.

The project follows a tutorial made by Tomas Sedovic, who is the author of the libtcod crate, to learn how to implement certain features using libtcod, but most of the code are typed and are original in implementations. Unfortunately I forgot to create commit histories for the time I was creating the base game without the additional features and was unable to figure out a way to retrieve those.

## What's Built

All of the essentials were built in terms of creating a generator for dungeons, having monsters attack, a lamp, win/lose conditions, items to use, and a GUI for the player to interact with. Additional items were implemented and there are more graphical interfaces that involves the use of mouse events, such as having a main menu and being able to see the name of the objects when hovering over the object with the mouse.

## How it Works

The project involves a procedural approach in generating the dungeon, where it first creates a room for the dungeon and then places whatever and how many objects it wants before creating the next room and connecting it with the previous room. This will indiscriminately overlap paths and create much larger corridors or crossroads from time to time. Once that is complete the player is able to use keyboard events to control the actions of the player object in the game. Both the player and monsters utilize the same initializations in which they are identified as a fighter. This fighter identity holds information in which they are living and are capable of hurting others. But since the player cannot attack anyone, only the monsters can attack. Much of the GUI involves positioning within the window itself and making sure it wraps in the interfaces correctly, such as the inventory being able to resize on each item added.

## What Didn't Work

I wanted to try and make it so that monsters would teleport elsewhere once it attacks the player, but I found out that it would only add more difficulty for one particular room. For example of I was attacked by a monster, that monster could end up being the 10th monster in a room. Additionally I wanted to try optimizing the numpad controls so that the input would have latency or feel laggy when held down, unfortunately I couldn't figure out a method in reducing that latency.

## Lessons Learned

I underestimated the capacity of the project, and had to work on the project everyday. I learned to create some sort of schedule and implement a sort of sprint for myself to get some sort of feature done within a week. Additionally I should've looked at the project description more carefully because I forgot to do my commits and lost much of my history of creating the base of the project, despite what my commit history says. The part I forgot to record is pretty much me creating the movement, implementing the monsters, creating a method to heal, and creating the map generator.
