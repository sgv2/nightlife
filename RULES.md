# Instructions to Play the Game

To run the game, simply run "cargo run --release" in the nightlife directory without the quotation marks in the terminal and interact the game with keyboard inputs when prompted.

## Player Controls

- Exit menu/Exit current game: Esc
- Fullscreen toggle: Alt + Enter

- Wait: numpad 5
- Move up: numpad 8 or up arrow
- Move down: numpad 2 or down arrow
- Move left: numpad 4 or left arrow
- Move right: numpad 6 or right arrow

- Move diagonally up right: numpad 9 or PageUp
- Move diagonally up left: numpad 7 or Home
- Move diagonally down right: numpad 3 or PageDown
- Move diagonally down left: numpad 1 or End

- Grab item: key F
- Use item: key D
- Drop item: key E
- Go down stairs: Spacebar

## Items

- Lamp Fuel: partially refuel lamp
- Flashbang Grenade: Overpower a monster with an explosion of light
- Blackout Bomb: Temporarily renders a monster from sensing light
